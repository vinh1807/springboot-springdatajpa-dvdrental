package com.dvdrental.controller;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dvdrental.entity.Actor;
import com.dvdrental.entity.Category;
import com.dvdrental.entity.Film;
import com.dvdrental.entity.Inventory;
import com.dvdrental.service.FilmService;

@RestController
@RequestMapping("film")
public class FilmController {

	@Autowired
	private FilmService filmService;

	@PostMapping("/")
	public ResponseEntity<?> saveFilm(@RequestBody Film film) {
		filmService.saveFilm(film);
		return ResponseEntity.ok().body(film);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Film> getFilmById(@PathVariable("id") int id) {
		Film film = filmService.getFilmById(id);
		return ResponseEntity.ok().body(film);
	}
	
	@GetMapping("/title/{titleOrDescription}")
	public ResponseEntity<List<Film>> getFilmByTitle(@PathVariable("titleOrDescription") String titleOrDescription) {
		List<Film> films = filmService.getFilmsByTitle(titleOrDescription);
		for (Film film : films) {
			film.setActors(new HashSet<Actor>());
			film.setInventories(new HashSet<Inventory>());
			film.setCategories(new HashSet<Category>());
		}
		return ResponseEntity.ok().body(films);
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Film>> getAllFilms() {
		List<Film> films = filmService.getAllFilms();
		for (Film film : films) {
			film.setActors(new HashSet<Actor>());
			film.setInventories(new HashSet<Inventory>());
			film.setCategories(new HashSet<Category>());
		}
		return ResponseEntity.ok().body(films);
	}

	@GetMapping("/")
	public ResponseEntity<Page<Film>> getAllFilmsByPage(@RequestParam(value = "orderBy", defaultValue = "filmId") String orderBy,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
		Page<Film> films = filmService.getAllFilmsByPage(orderBy, page, pageSize);
		for (Film film : films.getContent()) {
			film.setActors(new HashSet<Actor>());
			film.setInventories(new HashSet<Inventory>());
			film.setCategories(new HashSet<Category>());
		}
		return ResponseEntity.ok().body(films);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteFilm(@PathVariable("id") int filmId) {
		filmService.deleteFilm(filmId);
		return ResponseEntity.ok().body(filmId);
	}

	@PutMapping("/")
	public ResponseEntity<?> update(@RequestBody Film film) {
		filmService.updateFilm(film);
		return ResponseEntity.ok().body(film);
	}
}
