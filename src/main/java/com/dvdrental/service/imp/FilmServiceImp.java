package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.FilmDao;
import com.dvdrental.entity.Film;
import com.dvdrental.service.FilmService;
import com.google.common.collect.Lists;

@Service
public class FilmServiceImp implements FilmService {
	
	@Autowired
	FilmDao filmDao;
	
	@Transactional
	public void saveFilm(Film film) {
		filmDao.save(film);
	}

	@Transactional
	public List<Film> getAllFilms() {
		return Lists.newArrayList(filmDao.findAll(Sort.by("filmId")));
	}
	
	@Transactional
	public Page<Film> getAllFilmsByPage(String orderBy, int page, int pageSize) {
		return filmDao.findAll(PageRequest.of(page, pageSize, Sort.by(orderBy)));
	}

	@Transactional
	public Film getFilmById(int id) {
		return filmDao.findById(new Integer(id)).get();
	}

	@Transactional
	public void updateFilm(Film film) {
		filmDao.save(film);
	}

	@Transactional
	public void deleteFilm(int filmId) {
		filmDao.delete(filmDao.findById(new Integer(filmId)).get());
	}

	@Transactional
	public List<Film> getFilmsByTitle(String titleOrDescription) {
		return filmDao.findByTitleContainingOrDescriptionContainingAllIgnoreCase(titleOrDescription, titleOrDescription, Sort.by("filmId"));
	}

}
