package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.ActorDao;
import com.dvdrental.entity.Actor;
import com.dvdrental.service.ActorService;
import com.google.common.collect.Lists;

@Service
public class ActorServiceImp implements ActorService {
	
	@Autowired
	ActorDao actorDao;
	
	@Transactional
	public void saveActor(Actor actor) {
		actorDao.save(actor);
	}

	@Transactional
	public List<Actor> getAllActors() {	 
		return Lists.newArrayList(actorDao.findAll());
	}
	
	@Transactional
	public Page<Actor> getAllActorsByPage(String orderBy, int page, int pageSize) {
		return actorDao.findAll(PageRequest.of(page, pageSize, Sort.by(orderBy)));
	}

	@Transactional
	public Actor getActorById(int id) {
		return actorDao.findById(new Integer(id)).get();
	}
	
	@Transactional
	public void updateActor(Actor actor) {
		actorDao.save(actor);
	}

	@Transactional
	public void deleteActor(int actorId) {
		actorDao.delete(actorDao.findById(actorId).get());
	}

	@Transactional
	public List<Actor> getActorByName(String name) {
		return actorDao.findByFirstNameContainingOrLastNameContainingAllIgnoreCase(name, name);
	}

}
