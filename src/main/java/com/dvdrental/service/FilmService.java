package com.dvdrental.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.dvdrental.entity.Film;

public interface FilmService {

	void 			saveFilm(Film film);
	
	List<Film> 		getAllFilms();
	
	Page<Film> 		getAllFilmsByPage(String orderBy, int page, int pageSize);
	
	Film			getFilmById(int id);
	
	List<Film> 		getFilmsByTitle(String title);
	
	void 			updateFilm(Film film);
	
	void 			deleteFilm(int filmId);
	
}
