package com.dvdrental.dao;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.dvdrental.entity.Film;

public interface FilmDao extends PagingAndSortingRepository<Film, Integer> {

	List<Film> 	findByTitleContainingOrDescriptionContainingAllIgnoreCase(String title, String description, Sort sort);
	
}
