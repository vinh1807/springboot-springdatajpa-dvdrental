package com.dvdrental.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.dvdrental.entity.Actor;

public interface ActorDao extends PagingAndSortingRepository<Actor, Integer> {
	
	List<Actor> findByFirstNameContainingOrLastNameContainingAllIgnoreCase(String firstName, String lastName);

}
